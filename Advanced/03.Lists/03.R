util <- read.csv(file.choose())
head(util,20)
str(util)
summary(util)
#Derive utilization column
util$Utilization = 1 - util$Percent.Idle

#Handling Date-Times in R (Converting time into POSIX)
?POSIXct
util$PosixTime <- as.POSIXct(util$Timestamp, format = "%d/%m/%Y %H:%M")
summary(util)
#We don't need time stamp anymore, let's delete it and move posixtime
util$Timestamp <- NULL
util <- util[,c(4,1,2,3)]


#What is a list?
summary(util)
RL1 <- util[util$Machine == "RL1",]
summary(RL1)
RL1$Machine <- factor(RL1$Machine) #Removing "memory" of every other machine from previus factor

#Construct list

util_stats_rl1 <- c(min(RL1$Utilization, na.rm=TRUE),
                    mean(RL1$Utilization, na.rm=TRUE),
                    max(RL1$Utilization, na.rm=TRUE))

length(which(RL1$Utilization < 0.90))
util_under_90 <- length(which(RL1$Utilization < 0.90)) > 0

list_rl1 <- list("RL1",util_stats_rl1,util_under_90)

#naming compoments of the list

names(list_rl1)
names(list_rl1) <- c("Machine ", "Stats","Low Threshold")

#another way
rm(list_rl1)
list_rl1 <- list(Machine = "RL1",Stats = util_stats_rl1,LowThreshold = util_under_90) #Can't get space's that easy

#extracting compoments of the list
#three ways:
#[] - will always retrun a list
#[[]] - will always return actual object
#$ - same as [[]] but prettier

list_rl1
list_rl1[1]
list_rl1[[1]]
list_rl1$Machine


list_rl1[2]
typeof(list_rl1[2])
list_rl1[[2]]
typeof(list_rl1[[2]])
list_rl1$Stats
typeof(list_rl1$Stats)

#Accesing 3rd element of Stats:
list_rl1
list_rl1[[2]][3]
list_rl1$Stats[3]

list_rl1$LowThreshold

#addind and deleting compoments

list_rl1 
#list_rl1[4] <- NULL
list_rl1

#another thing to add to the list 
#Vector with all the hours where utilization is (NA)
list_rl1$UknownHours <- RL1[is.na(RL1$Utilization),"PosixTime"]
#Numeration will be shifted after deleting list position before the last

#another compoment - Data frame for this machine 

list_rl1$Data <- RL1
summary(list_rl1)
str(list_rl1)

#subsetting list
list_rl1$UknownHours[1]

list_rl1[1]
list_rl1[1:3]
list_rl1[c(1,4)]
list_rl1[c("Machine","Stats")]

#buliding a timeseries plot
library("ggplot2")

p <- ggplot(data=util)
p + geom_line(aes(x = PosixTime, y = Utilization, color = Machine ), size = 1.2) +
  facet_grid(Machine~.) + 
  geom_hline(yintercept = 0.90, alpha = 0.6, size = 1.2, linetype = "dotted")


myplot <- p + geom_line(aes(x = PosixTime, y = Utilization, color = Machine ), size = 1.2) +
  facet_grid(Machine~.) + 
  geom_hline(yintercept = 0.90, alpha = 0.6, size = 1.2, linetype = "dotted")

myplot

list_rl1$Plot <- myplot
