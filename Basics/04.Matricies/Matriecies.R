Salary
Salary[3,]
Salary[,3]
Games
MinutesPlayed


#matrix() #matrixbind(when we have 3 rows and 3 columns and insert 5 records they will be inserted into [1,1][1,2][1,3][2,1][2,2], wchich is kinda bad)
?matrix()
my.data <- 1:20
my.data

A<- matrix(my.data,4,5)
A
A[1,]
A[,3]

B<- matrix(my.data,4,5, byrow=T)
B
B[2,5]


#rbind() #rowbind -> rowbind

r1 <- c("I","am","happy")
r2 <- c("What", "a", "day")
r3 <- c(1,2,3)
rbind(r1,r2,r3)
#cbind() #column bind ^ column bind

c1 <- 1:5
c2 <- -1:-5
cbind(c1,c2)

#Named Vectors

Charlie <- 1:5
Charlie

#Give Names
names(Charlie)
names(Charlie) <- c("a","b","c","d","e")
Charlie
Charlie["d"]

#Clear Names 
names(Charlie) <- NULL
Charlie

#Naming Matrix Dimensions 1

temp.vec1 <- rep(c("a","B","zZ"),each = 3)
temp.vec <- rep(c("a","B","zZ"),3)
c("a","B","zZ")

Bravo1 <- matrix(temp.vec,3,3)
Bravo1
Bravo2 <- matrix(temp.vec1,3,3)
Bravo2
rownames(Bravo2) <- c(1,2,3)
colnames(Bravo2) <- c("X","Y","Z")

Bravo2[2,"Y"]
rownames(Bravo2) <- NULL
colnames(Bravo2) <- NULL
Bravo2

#-----------------------------

Games
rownames(Games)
colnames(Games)
Games["LeBronJames","2012"]

FieldGoals
FieldGoalAttempts

round(FieldGoals/Games,1)
round(MinutesPlayed/Games,1)

#------------------
?matplot()

matplot(t(FieldGoals/Games), type="b", pch = 15:18, col = c(1:4,6))
legend("bottomleft", inset=0.01, legend = Players, col = c(1:4,6),pch = 15:18, horiz = FALSE)

FieldGoals
t(FieldGoals) #transpose



matplot(t(FieldGoals/FieldGoalAttempts), type="b", pch = 15:18, col = c(1:4,6))
legend("bottomleft", inset=0.01, legend = Players, col = c(1:4,6),pch = 15:18, horiz = FALSE)

#-----------------------------


x <- c("a","b","c","d","e")
x
x[c(1,5,4)]
x[1]

Games
Games[1:3,6:10]
Games[c(1,10),]
Games[,c("2008","2009")]

Games[1,,drop=FALSE]
Games[1,]

#------------------------------




data <- MinutesPlayed[1:3,]
matplot(t(data), type="b", pch = 15:18, col = c(1:4,6))
legend("bottomleft", inset=0.01, legend = Players[1:3], col = c(1:4,6),pch = 15:18, horiz = FALSE)

myplot <- function(type,rows=1:10){
  data <- type[rows,,drop = FALSE]
  matplot(t(data), type="b", pch = 15:18, col = c(1:4,6))
}

myplot <- function(type,rows=1:10){
  data <- type[rows,,drop = FALSE]
  matplot(t(data), type="b", pch = 15:18, col = c(1:4,6))
  legend("bottomleft", inset=0.01, legend = Players[rows], col = c(1:4,6),pch = 15:18, horiz = FALSE)
}
myplot(Games)

#--------------------------

#salary
myplot(Salary)
myplot(Salary/Games)
myplot(Salary/FieldGoals)

#In-Game
myplot(MinutesPlayed)
myplot(Points)

#In-Game Normalized
myplot(FieldGoals/Games)
myplot(FieldGoals/FieldGoalAttempts)
myplot(FieldGoalAttempts/Games)
myplot(Points/Games)

#Observations
myplot(MinutesPlayed/Games)
myplot(Games)

#How valuable time
myplot(FieldGoals/MinutesPlayed)

#Player Style
myplot(Points/FieldGoals)
