?read.csv()

#Method 1: Select The File Manually

Stats <- read.csv(file.choose())

Stats

#Method 2: Set WD and Read Data

getwd()
#Windows
setwd("C:\\Users\\Rafal\\Desktop\\Develop\\Atom\\r-coding") 

#--------------- Exploring Data

Stats

nrow(Stats) #rows
ncol(Stats) #colums
head(Stats, n = 10)
tail(Stats, n = 8)
str(Stats) #str() runif()
summary(Stats)

Stats[3,3]
Stats[3,"Birth.rate"]
Stats[,3]
Stats$Internet.users
Stats$Internet.users[2]
Stats[,"Internet.users"]
levels(Stats$Country.Name)

#----------- Basic DF operations

Stats[1:10,]
Stats[3:9,]
Stats[c(4,100),]
#remember how [] work:
Stats[1,]
is.data.frame(Stats[1,])
Stats[,1,drop=F]
is.data.frame(Stats[,1,drop=F])
#mutiply columns
head(Stats)
Stats$Birth.rate * Stats $Internet.users
Stats$Birth.rate + Stats $Internet.users
#add column
head(Stats)
Stats$MyCol <- Stats$Birth.rate * Stats $Internet.users
#test of knowledge
Stats$xyz <- 1:4 #195 nie podzielne przez 4
Stats$xyz <- 1:5
Stats

#remove
Stats$xyz <- NULL
Stats$MyCol <- NULL

#--------------------------------- Filtering Data Frames
head(Stats)
filter <- Stats$Internet.users < 2
Stats[filter,]

Stats[Stats$Birth.rate >40,]
Stats[Stats$Birth.rate >40 & Stats$Internet.users < 2 ,]
Stats[Stats$Income.Group == "High income",]
levels(Stats$Income.Group)
Stats[Stats$Country.Name == "Malta",]


#----------------------- Introdution to qplot()
library(ggplot2)

?qplot() 
qplot(data = Stats, x =Internet.users, y=Birth.rate)
qplot(data = Stats, x =Internet.users)
qplot(data = Stats, x =Income.Group, y=Internet.users, size = I(3))
qplot(data = Stats, x =Income.Group, y=Internet.users, size = I(3), color = I("Blue"))
qplot(data = Stats, x =Income.Group, y=Internet.users, size = I(3), geom ="boxplot", color = I("Blue"))


#----------------------- Visualizing with Qplot part 1

qplot(data = Stats, x =Internet.users, y=Birth.rate)
qplot(data = Stats, x =Internet.users, y=Birth.rate, size=I(4)) 
qplot(data = Stats, x =Internet.users, y=Birth.rate, size=I(4), color = I("red"))
qplot(data = Stats, x =Internet.users, y=Birth.rate, size=I(2), color = Income.Group)


#----------------------- Creating Data Frames

#Execute below code to generate three new vectors
Countries_2012_Dataset <- c("Aruba","Afghanistan","Angola","Albania","United Arab Emirates","Argentina","Armenia","Antigua and Barbuda","Australia","Austria","Azerbaijan","Burundi","Belgium","Benin","Burkina Faso","Bangladesh","Bulgaria","Bahrain","Bahamas, The","Bosnia and Herzegovina","Belarus","Belize","Bermuda","Bolivia","Brazil","Barbados","Brunei Darussalam","Bhutan","Botswana","Central African Republic","Canada","Switzerland","Chile","China","Cote d'Ivoire","Cameroon","Congo, Rep.","Colombia","Comoros","Cabo Verde","Costa Rica","Cuba","Cayman Islands","Cyprus","Czech Republic","Germany","Djibouti","Denmark","Dominican Republic","Algeria","Ecuador","Egypt, Arab Rep.","Eritrea","Spain","Estonia","Ethiopia","Finland","Fiji","France","Micronesia, Fed. Sts.","Gabon","United Kingdom","Georgia","Ghana","Guinea","Gambia, The","Guinea-Bissau","Equatorial Guinea","Greece","Grenada","Greenland","Guatemala","Guam","Guyana","Hong Kong SAR, China","Honduras","Croatia","Haiti","Hungary","Indonesia","India","Ireland","Iran, Islamic Rep.","Iraq","Iceland","Israel","Italy","Jamaica","Jordan","Japan","Kazakhstan","Kenya","Kyrgyz Republic","Cambodia","Kiribati","Korea, Rep.","Kuwait","Lao PDR","Lebanon","Liberia","Libya","St. Lucia","Liechtenstein","Sri Lanka","Lesotho","Lithuania","Luxembourg","Latvia","Macao SAR, China","Morocco","Moldova","Madagascar","Maldives","Mexico","Macedonia, FYR","Mali","Malta","Myanmar","Montenegro","Mongolia","Mozambique","Mauritania","Mauritius","Malawi","Malaysia","Namibia","New Caledonia","Niger","Nigeria","Nicaragua","Netherlands","Norway","Nepal","New Zealand","Oman","Pakistan","Panama","Peru","Philippines","Papua New Guinea","Poland","Puerto Rico","Portugal","Paraguay","French Polynesia","Qatar","Romania","Russian Federation","Rwanda","Saudi Arabia","Sudan","Senegal","Singapore","Solomon Islands","Sierra Leone","El Salvador","Somalia","Serbia","South Sudan","Sao Tome and Principe","Suriname","Slovak Republic","Slovenia","Sweden","Swaziland","Seychelles","Syrian Arab Republic","Chad","Togo","Thailand","Tajikistan","Turkmenistan","Timor-Leste","Tonga","Trinidad and Tobago","Tunisia","Turkey","Tanzania","Uganda","Ukraine","Uruguay","United States","Uzbekistan","St. Vincent and the Grenadines","Venezuela, RB","Virgin Islands (U.S.)","Vietnam","Vanuatu","West Bank and Gaza","Samoa","Yemen, Rep.","South Africa","Congo, Dem. Rep.","Zambia","Zimbabwe")
Codes_2012_Dataset <- c("ABW","AFG","AGO","ALB","ARE","ARG","ARM","ATG","AUS","AUT","AZE","BDI","BEL","BEN","BFA","BGD","BGR","BHR","BHS","BIH","BLR","BLZ","BMU","BOL","BRA","BRB","BRN","BTN","BWA","CAF","CAN","CHE","CHL","CHN","CIV","CMR","COG","COL","COM","CPV","CRI","CUB","CYM","CYP","CZE","DEU","DJI","DNK","DOM","DZA","ECU","EGY","ERI","ESP","EST","ETH","FIN","FJI","FRA","FSM","GAB","GBR","GEO","GHA","GIN","GMB","GNB","GNQ","GRC","GRD","GRL","GTM","GUM","GUY","HKG","HND","HRV","HTI","HUN","IDN","IND","IRL","IRN","IRQ","ISL","ISR","ITA","JAM","JOR","JPN","KAZ","KEN","KGZ","KHM","KIR","KOR","KWT","LAO","LBN","LBR","LBY","LCA","LIE","LKA","LSO","LTU","LUX","LVA","MAC","MAR","MDA","MDG","MDV","MEX","MKD","MLI","MLT","MMR","MNE","MNG","MOZ","MRT","MUS","MWI","MYS","NAM","NCL","NER","NGA","NIC","NLD","NOR","NPL","NZL","OMN","PAK","PAN","PER","PHL","PNG","POL","PRI","PRT","PRY","PYF","QAT","ROU","RUS","RWA","SAU","SDN","SEN","SGP","SLB","SLE","SLV","SOM","SRB","SSD","STP","SUR","SVK","SVN","SWE","SWZ","SYC","SYR","TCD","TGO","THA","TJK","TKM","TLS","TON","TTO","TUN","TUR","TZA","UGA","UKR","URY","USA","UZB","VCT","VEN","VIR","VNM","VUT","PSE","WSM","YEM","ZAF","COD","ZMB","ZWE")
Regions_2012_Dataset <- c("The Americas","Asia","Africa","Europe","Middle East","The Americas","Asia","The Americas","Oceania","Europe","Asia","Africa","Europe","Africa","Africa","Asia","Europe","Middle East","The Americas","Europe","Europe","The Americas","The Americas","The Americas","The Americas","The Americas","Asia","Asia","Africa","Africa","The Americas","Europe","The Americas","Asia","Africa","Africa","Africa","The Americas","Africa","Africa","The Americas","The Americas","The Americas","Europe","Europe","Europe","Africa","Europe","The Americas","Africa","The Americas","Africa","Africa","Europe","Europe","Africa","Europe","Oceania","Europe","Oceania","Africa","Europe","Asia","Africa","Africa","Africa","Africa","Africa","Europe","The Americas","The Americas","The Americas","Oceania","The Americas","Asia","The Americas","Europe","The Americas","Europe","Asia","Asia","Europe","Middle East","Middle East","Europe","Middle East","Europe","The Americas","Middle East","Asia","Asia","Africa","Asia","Asia","Oceania","Asia","Middle East","Asia","Middle East","Africa","Africa","The Americas","Europe","Asia","Africa","Europe","Europe","Europe","Asia","Africa","Europe","Africa","Asia","The Americas","Europe","Africa","Europe","Asia","Europe","Asia","Africa","Africa","Africa","Africa","Asia","Africa","Oceania","Africa","Africa","The Americas","Europe","Europe","Asia","Oceania","Middle East","Asia","The Americas","The Americas","Asia","Oceania","Europe","The Americas","Europe","The Americas","Oceania","Middle East","Europe","Europe","Africa","Middle East","Africa","Africa","Asia","Oceania","Africa","The Americas","Africa","Europe","Africa","Africa","The Americas","Europe","Europe","Europe","Africa","Africa","Middle East","Africa","Africa","Asia","Asia","Asia","Asia","Oceania","The Americas","Africa","Europe","Africa","Africa","Europe","The Americas","The Americas","Asia","The Americas","The Americas","The Americas","Asia","Oceania","Middle East","Oceania","Middle East","Africa","Africa","Africa","Africa")

mydf <- data.frame(Countries_2012_Dataset,Codes_2012_Dataset,Regions_2012_Dataset)

#head(mydf)
#colnames(mydf) <-c("Country","Code","Regions")
rm(mydf)

mydf <- data.frame(Country=Countries_2012_Dataset,Code=Codes_2012_Dataset,Regions = Regions_2012_Dataset)
head(mydf)
tail(mydf)
summary(mydf)

#-------------- Merging dataframes
head(Stats)
head(mydf)
mydf$Birth.rate <- Stats$Birth.rate
mydf$Internet.users <- Stats$Internet.users
mydf$Income.Group <- Stats$Income.Group
mydf

merged <- merge(Stats,mydf, by.x = "Country.Code", by.y = "Code")
head(merged)

merged$Country <- NULL
str(merged)
head(merged)


#-------------------------------- Visualizing with new Split

qplot(data = merged, x =Internet.users, y=Birth.rate, size=I(2), color = Regions)

#1. Shapes

qplot(data = merged, x =Internet.users, y=Birth.rate, size=I(2), color = Regions, shape = I(17))

#2. Transparency

qplot(data = merged, x =Internet.users, y=Birth.rate, size=I(2), color = Regions, alpha = I(0.4) )

#3. Title
qplot(data = merged, x =Internet.users, y=Birth.rate, size=I(2), color = Regions, alpha = I(0.4), main = "Birth rate vs internet" )